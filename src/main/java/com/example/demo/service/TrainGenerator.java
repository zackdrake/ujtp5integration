package com.example.demo.service;

import com.example.demo.model.Train;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

public interface TrainGenerator {

    public List<Train> getTrains();
}
