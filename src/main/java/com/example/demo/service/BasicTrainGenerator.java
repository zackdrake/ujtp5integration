package com.example.demo.service;

import com.example.demo.model.Train;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@ConditionalOnProperty(value = "mode", havingValue = "basic", matchIfMissing = true)
public class BasicTrainGenerator implements TrainGenerator {

    @Override
    public List<Train> getTrains() {
        Train train = new Train();
        train.setDestination("Paris");
        List<Train> trains = new ArrayList<>();
        trains.add(train);
        return trains;
    }
}
