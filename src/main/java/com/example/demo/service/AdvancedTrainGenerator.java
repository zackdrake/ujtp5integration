package com.example.demo.service;

import com.example.demo.model.Train;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@ConditionalOnProperty(value = "mode", havingValue = "advanced")
public class AdvancedTrainGenerator implements TrainGenerator {

    @Override
    public List<Train> getTrains() {
        Train train = new Train();
        train.setDestination("Paris");
        Train train2 = new Train();
        train.setDestination("Caen");
        List<Train> trains = new ArrayList<>();
        trains.add(train);
        trains.add(train2);
        return trains;
    }
}
