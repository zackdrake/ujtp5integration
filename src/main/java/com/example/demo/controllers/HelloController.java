package com.example.demo.controllers;

import com.example.demo.dao.SNCFApiDAO;
import com.example.demo.model.Train;
import com.example.demo.service.TrainGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class HelloController {

    @Autowired
    private TrainGenerator trainGenerator;

    @Autowired
    private SNCFApiDAO sncfApiDAO;

    @GetMapping("/train")
    public List<Train> hello() {
        System.out.println(sncfApiDAO.fetchData().getPagination().getItemsPerPage());
        return trainGenerator.getTrains();
    }
}
